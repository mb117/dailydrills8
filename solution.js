const cart = require("./3-cart.js");

// console.log(cart);

// Q1. Find all the items with price more than $65.

function question1(cart) {

    const result = cart.reduce((result, items) => {
        let temp = Object.keys(items).reduce((result, item) => {
            let price;

            if ( Array.isArray(items[item]) === false) {
                price = Number(items[item].price.substring(1));

                if ( price > 65) {
                    result.push(item);
                }

            } else {
                items[item].map((groupedItem) => {
                    price = Number(groupedItem[Object.keys(groupedItem)[0]].price.substring(1));
                    
                    if ( price > 65) {
                        result.push(Object.keys(groupedItem)[0]);
                    }

                    return;
                });
            }

            return result;
        }, []);

        result.push(...temp);

        return result;
    }, []);

    return result;
}

// console.log(question1(cart));


// Q2. Find all the items where quantity ordered is more than 1.
function question2(cart) {

    const result = cart.reduce((result, items) => {
        let temp = Object.keys(items).reduce((result, item) => {
            let quantity;

            if ( Array.isArray(items[item]) === false) {
                quantity = items[item].quantity;

                if ( quantity > 1) {
                    result.push(item);
                }

            } else {
                items[item].map((groupedItem) => {
                    quantity = groupedItem[Object.keys(groupedItem)[0]].quantity;
                    
                    if ( quantity > 1) {
                        result.push(Object.keys(groupedItem)[0]);
                    }

                    return;
                });
            }

            return result;
        }, []);

        result.push(...temp);

        return result;
    }, []);

    return result;
}

// console.log(question2(cart));

// Q.3 Get all items which are mentioned as fragile.
function question3(cart) {

    const result = cart.reduce((result, items) => {
        let temp = Object.keys(items).reduce((result, item) => {
            if ( Array.isArray(items[item]) === false) {

                if ( items[item].hasOwnProperty("type") === true && items[item]["type"] === "fragile") {
                    result.push(item);
                }

            } else {
                items[item].map((groupedItem) => {
                    if (groupedItem[Object.keys(groupedItem)[0]].hasOwnProperty("type") &&
                    groupedItem[Object.keys(groupedItem)[0]]["type"] === "fragile") {
                        result.push(Object.keys(groupedItem)[0]);
                    }

                    return;
                });
            }

            return result;
        }, []);

        result.push(...temp);

        return result;
    }, []);

    return result;
}

// console.log(question3(cart));

// Q.4 Find the least and the most expensive item for a single quantity.


function question4(cart) {
    let max = ["",0];
    let min = ["", Infinity];

    cart.reduce((result, items) => {
        let temp = Object.keys(items).reduce((result, item) => {
            let price;

            if ( Array.isArray(items[item]) === false) {
                price = Number(items[item].price.substring(1)) / items[item].quantity;

                if ( price > max[1]) {
                    max = [item, price];
                } else if ( price < min[1]) {
                    min = [items[item], price];
                }

            } else {
                items[item].map((groupedItem) => {
                    price = Number(groupedItem[Object.keys(groupedItem)[0]].price.substring(1)) / groupedItem[Object.keys(groupedItem)[0]].quantity;
                    const name = Object.keys(groupedItem)[0];

                    if (price > max[1]) {
                        max = [name, price];
                    } else if (price < min[1]) {
                        min = [name, price];
                    }

                    return;
                });
            }

            return result;
        }, []);

        result.push(...temp);

        return result;
    }, []);

    return [max, min];
}

// console.log(question4(cart));


// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)
function question5(cart) {
    const statesOfMatter = {
        "solid":  ["comb", "spoons", "glasses", "cooker", "watch"],
        "liquid": ["shampoo", "Hair-oil", ],
        "gas": []
    };
    const result = {
        "solid": [],
        "liquid": [],
        "gas": []
    };

    cart.reduce((_, items) => {
        Object.keys(items).reduce((_, item) => {

            if ( Array.isArray(items[item]) === false) {
                if ( statesOfMatter["solid"].indexOf(item) > -1) {
                    result["solid"].push(item);
                } else if ( statesOfMatter["liquid"].indexOf(item) > -1) {
                    result["liquid"].push(item);
                } else {
                    result["gas"].push(item);
                }

            } else {
                items[item].map((groupedItem) => {
                    price = Number(groupedItem[Object.keys(groupedItem)[0]].price.substring(1));
                    
                    const propertyName = Object.keys(groupedItem)[0];

                    if ( statesOfMatter["solid"].indexOf(propertyName) > -1) {
                        result["solid"].push(propertyName);
                    } else if ( statesOfMatter["liquid"].indexOf(propertyName) > -1) {
                        result["liquid"].push(propertyName);
                    } else {
                        result["gas"].push(propertyName);
                    }

                    return;
                });
            }

            return;
        }, []);


        return;
    }, []);

    return result;
}

console.log(question5(cart));